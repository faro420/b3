package b3;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
public class Counter {
    private int number;

    public Counter(int n) {
        number = n;
    }
    public Counter() {
        this(0);
    }


    public void inc() {
        number++;
    }

    public int getNumber() {
        return number;
    }

    public String toString() {
        return String.format("[<%s>]:%d", Counter.class.getSimpleName(), number);
    }
}
