package b3;
/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class PappagalloPlusPlus {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        long before = System.nanoTime();

        WordGrabber wg = new WordGrabber("e://test.txt");
        HashMap<Word, Counter> map = new HashMap<Word, Counter>();
        
        while (wg.hasNext()) {
            final String currentRawWord = wg.next().toLowerCase();
            final Word currentWord = new Word(currentRawWord);
            final Counter currentCounter = map.get(currentWord);
            if (currentCounter != null)
                currentCounter.inc();
            else {
                map.put(currentWord, new Counter(1));
            }
        }
        ArrayList<Word> list = new ArrayList<Word>();
        list.addAll(map.keySet());
        Collections.sort(list);

        for (int i = 0; i < list.size(); i++) {
            System.out.printf("[ %s ] : %d\n", list.get(i).getWord(), map.get(list.get(i)).getNumber());
        }
        long after = System.nanoTime();
        System.out.println(after - before);
    }
}
