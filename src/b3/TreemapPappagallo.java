package b3;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class TreemapPappagallo {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        long before = System.nanoTime();
     
        WordGrabber wg = new WordGrabber("e://test.txt");
        Map<Word, Counter> map = new TreeMap<Word, Counter>();
        Word wpointer;
        Counter cpointer;
        String s = "";
        if (wg.hasNext()) {
            wpointer = new Word(wg.next().toLowerCase());
            map.put(wpointer, new Counter());
            map.get(wpointer).inc();
        }
        while (wg.hasNext()) {
            s = wg.next().toLowerCase();
            wpointer = new Word(s);
            cpointer = map.get(wpointer);
            if (cpointer != null)
                cpointer.inc();
            else {
                map.put(wpointer, new Counter());
                map.get(wpointer).inc();
            }
        }
        for (Map.Entry<Word, Counter> entry : map.entrySet()) {
            System.out.printf("[ %s ] : %d\n", entry.getKey().getWord(), entry.getValue().getNumber());
        }
        long after = System.nanoTime();
        System.out.println(after - before); 
    }
}
