package b3;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 * @param <T>
 */
public class Word implements Comparable<Word> {
    private String word;

    public Word(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    @Override
    public boolean equals(Object other) {
        return getWord().equals(((Word) other).getWord());
    }

    @Override
    public int hashCode() {
        return word.hashCode();
    }

    @Override
    public String toString() {
        return String.format("[<%s>]:%s", Word.class.getSimpleName(), word);
    }

    public int compareTo(Word w) {
       return word.compareTo(w.getWord());
    }
}
